const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const q = [
  'Cual es tu primer nombre? ',
  'Cual es tu primer apellido? ',
  'Cual es tu edad? '
];

const AskQuestion = (rl, question) => {
  return new Promise((resolve, rej) => {
    rl.question(question, answer => {
      resolve(answer);
    });
  });
}

const Ask = (questions) => {
  return new Promise(async function(resolve,reject){
    console.log("inicia preguntas");
    let results = [];
    for (let i=0; i < questions.length; i++) {
      console.log("inicia pregunta: ",i);
      const result = await AskQuestion(rl, questions[i]);
      results = [...results, result];
      console.log("guarda respuesta");
    }
    rl.close();
    resolve(results);
  });
}

Ask(q)
  .then(q => {
    console.log(`Hola ${q[0]} ${q[1]}, tu edad es la siguiente: ${q[2]}`);
  })

// const Ask = (questions) => {
//   return new Promise(async resolve => {
//     let results = [];
//     for (let i=0; i < questions.length; i++) {
//       const result = await AskQuestion(rl, questions[i]);
//       results = [...results, result];
//     }
//     rl.close();
//     resolve(results);
//   });
// }


// askQ();
// async function askQ(){
//   const r = await Ask(q);
//   console.log(`Hola ${r[0]}${r[1]}, tu edad es la siguiente: ${r[2]}`);
// }

