const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const q = [
  'Cual es tu primer nombre? ',
  'Cual es tu primer apellido? ',
  'Cual es tu edad? '
];

const AskQuestion = (rl, question) => {
  
    rl.question(question, answer => {
      console.log("pregunta: ", question);
      return answer;
    });
}

const Ask = (questions) => {
    console.log("Inicia pregunta");
    let results = [];
    for (let i=0; i < questions.length; i++) {
      console.log("pregunta: ",questions[i]);
      const result = AskQuestion(rl, questions[i]);
      console.log("guarda respuesta");
      results = [...results, result];
    }
    rl.close();
    return results;
}

console.log(Ask(q));
