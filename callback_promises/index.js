const call = require("./src/call.js");

call.withCallback("ricardo",call.sync);

call.withPromise().then(function(name){
					console.log(name);
				})
		  .catch(error => console.log(error));

async function callWithPromise(){
	try{
		const fullName = await call.withPromise();
		console.log(fullName);
	}
	catch(error){
		console.log(error);
	}

}

callWithPromise();
