module.exports = (app) => {

	app.get("/", (req,res)=>{
		res.send("Ruta /");
	})

	app.get("/about", (req,res)=>{
		res.send("Ruta about");
	})

	app.get("*", (req,res)=>{
		res.send("Ruta random");
	})

}