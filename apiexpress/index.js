const express = require("express");
const app = express();
const { PORT } = require("./config");

require("./routes/api")(app);
require("./routes/views")(app);

function init(){
	console.log("Iniciando instancia de express.");
	app.listen(PORT,()=>{
		console.log("el servidor express está activo");
	});
}

init();