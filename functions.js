function a(){

	console.log("Function a");
};

function b(){
	
	setTimeout(function(){
		console.log("Function b");
	}
	,1000);

}

c =()=>{
	console.log("Function c");
}


d = function(){
	console.log("Function d");
}

e = (text) => console.log(`Function ${text}`);

a();
b();
c();
d();
e("e");