const express = require('express');

const app = express();

app.get("*",(req,res)=>{
	res.send("Random page");
})


app.get("/app",(req,res)=>{
	res.send("Estoy en la ruta app");
})

app.get("/",(req,res)=>{
	res.send("Estoy en la ruta HOME");
})

app.listen(3000, ()=>{

	console.log("El servidor express inició en el puerto 3000");

});
