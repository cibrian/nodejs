const math = require("./math");
const greet = require("./greetings");
const hello = require("./greetings/hello");

console.log(math.add(4,2));
console.log(math.substract(4,2));
console.log(math.multiply(4,2));
console.log(math.divide(4,2));

console.log(greet.greet("Ricardo"));
console.log(hello.sayHello("Ricardo"));